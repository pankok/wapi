import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspeciesPage } from './especies.page';

describe('EspeciesPage', () => {
  let component: EspeciesPage;
  let fixture: ComponentFixture<EspeciesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspeciesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspeciesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
