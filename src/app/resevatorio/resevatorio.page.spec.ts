import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResevatorioPage } from './resevatorio.page';

describe('ResevatorioPage', () => {
  let component: ResevatorioPage;
  let fixture: ComponentFixture<ResevatorioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResevatorioPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResevatorioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
